using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

public class Startup
{
    public void Configure(IApplicationBuilder app)
    {
        app.UseStaticFiles(); // Serve static files like index.html

        app.Run(async (context) =>
        {
            await context.Response.WriteAsync("Hello from the Console App!");
        });
    }
}
